FROM ubuntu
COPY ./app.py ./requirements.txt ./
RUN apt update
RUN apt install python3 python3-pip -y
RUN pip3 install -r requirements.txt
CMD /bin/python3
